package buu.thodsaporn.mathgame2

import android.graphics.Color
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.TextView
import androidx.activity.addCallback
import androidx.databinding.DataBindingUtil
import androidx.navigation.findNavController
import buu.thodsaporn.mathgame2.databinding.FragmentPlusGameBinding
import kotlinx.android.synthetic.main.fragment_plus_game.*
import kotlin.random.Random


/**
 * A simple [Fragment] subclass.
 * Use the [PlusGameFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class PlusGameFragment : Fragment() {
    // TODO: Rename and change types of parameters
    private lateinit var binding: FragmentPlusGameBinding
    private var menu = 0
    private var pointIncorrect = 0
    private var pointCorrect = 0

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        binding = DataBindingUtil.inflate<FragmentPlusGameBinding>(
            inflater,
            R.layout.fragment_plus_game,
            container,
            false)

        menu = PlusGameFragmentArgs.fromBundle(requireArguments()).menu
        pointCorrect = PlusGameFragmentArgs.fromBundle(requireArguments()).pointCorrect
        pointIncorrect = PlusGameFragmentArgs.fromBundle(requireArguments()).pointIncorrect
        setPoint(binding)
        play(binding)

        requireActivity().onBackPressedDispatcher.addCallback(this) {
            view?.findNavController()?.navigate(PlusGameFragmentDirections.actionPlusGameFragmentToTitleFragment(pointCorrect, pointIncorrect)
            )
        }

        println("menu "+menu)
        return binding.root
    }

    private fun play(binding: FragmentPlusGameBinding) {
        val result = setQuestion(binding)
        val btn1 = binding.btnAns1
        val btn2 = binding.btnAns2
        val btn3 = binding.btnAns3

        randomButton(result, btn1, btn2, btn3)

        checkClick(btn1, result, binding)
        checkClick(btn2, result, binding)
        checkClick(btn3, result, binding)
    }


    private fun checkClick(
        btn: Button,
        result: Int,
        binding: FragmentPlusGameBinding
    ) {
        btn.setOnClickListener {
            if (btn.text.toString().toInt() == result) {
                ansCorrect(txtAnswer)

            } else {
                ansIncorrect(txtAnswer, result)
            }
            setPoint(binding)
            play(binding)

        }
    }

    private fun setPoint(binding: FragmentPlusGameBinding) {
        binding.txtCorrect.text = getString(R.string.correct) + pointCorrect.toString()
        binding.txtIncorrect.text = getString(R.string.incorrect) + pointIncorrect.toString()
    }

    private fun ansIncorrect(txtAnswer: TextView, result: Int,) {
        txtAnswer.text = getString(R.string.incorrectTxt)
        txtAnswer.setTextColor(Color.parseColor("#ff0000"))
        pointIncorrect+=1
    }

    private fun ansCorrect(txtAnswer: TextView) {
        txtAnswer.text = getString(R.string.correctTxt)
        txtAnswer.setTextColor(Color.parseColor("#00ff00"))
        pointCorrect +=1
    }

    private fun randomButton(
        result: Int,
        btn1: Button,
        btn2: Button,
        btn3: Button
    ) {
        val randomNum = Random.nextInt(1, 4)
        val btnValue1 = (result + 1).toString()
        val btnValue2 = (result - 1).toString()
        if (randomNum == 1) {
            btn1.text = result.toString()
            btn2.text = btnValue1
            btn3.text = btnValue2

        } else if (randomNum == 2) {
            btn1.text = btnValue1
            btn2.text = result.toString()
            btn3.text = btnValue2
        } else {
            btn1.text = btnValue1
            btn2.text = btnValue2
            btn3.text = result.toString()
        }
    }

    private fun setQuestion(binding: FragmentPlusGameBinding): Int {
        val num1 = binding.num1
        val num2 = binding.num2
        val txtSign = binding.txtPlus
        var result : Int

        var number1 = Random.nextInt(0, 10)
        var number2 = Random.nextInt(0, 10)


        if (menu == 1){
            result = number1 + number2
            txtSign.text = getString(R.string.plus)

        }else if(menu == 2){
            result = number1 - number2
            txtSign.text = getString(R.string.minus)
        }else if (menu == 3) {
            result = number1 * number2
            txtSign.text = getString(R.string.multiple)
        }else {
            while (true){
                number2 = Random.nextInt(1,10)
                if (number1 % number2 ==0){
                    result = number1 / number2
                    txtSign.text = getString(R.string.divide)
                    break
                }
            }
        }
        num1.text = number1.toString()
        num2.text = number2.toString()
        return result
    }
}