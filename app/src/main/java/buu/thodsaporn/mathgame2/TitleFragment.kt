package buu.thodsaporn.mathgame2

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import buu.thodsaporn.mathgame2.databinding.FragmentTitleBinding
import androidx.navigation.findNavController as findNavController1

// TODO: Rename parameter arguments, choose names that match
// the fragment initialization parameters, e.g. ARG_ITEM_NUMBER


/**
 * A simple [Fragment] subclass.
 * Use the [TitleFragment.newInstance] factory method to
 * create an instance of this fragment.
 */
class TitleFragment : Fragment() {
    // TODO: Rename and change types of parameters

    private var menu = 0
    private var pointCorrect = 0
    private var pointIncorrect = 0


    override fun onCreateView(   inflater: LayoutInflater, container: ViewGroup?,
                                 savedInstanceState: Bundle?
    ): View? {
        val binding = DataBindingUtil.inflate<FragmentTitleBinding>(inflater, R.layout.fragment_title, container, false)

        pointCorrect = TitleFragmentArgs.fromBundle(requireArguments()).pointCorrect
        pointIncorrect = TitleFragmentArgs.fromBundle(requireArguments()).pointIncorrect

        binding.apply {
            playPlusGamebtn.setOnClickListener {
                menu = 1
                view?.findNavController1()?.navigate(TitleFragmentDirections.actionTitleFragmentToPlusGameFragment(pointCorrect,pointIncorrect,menu))
            }
            playMinusGamebtn.setOnClickListener {
                menu = 2
                view?.findNavController1()?.navigate(TitleFragmentDirections.actionTitleFragmentToPlusGameFragment(pointCorrect,pointIncorrect,menu))
            }
            playMultiGamebtn.setOnClickListener {
                menu = 3
                view?.findNavController1()?.navigate(TitleFragmentDirections.actionTitleFragmentToPlusGameFragment(pointCorrect,pointIncorrect,menu))
            }
            playDivideGamebtn.setOnClickListener {
                menu = 4
                view?.findNavController1()?.navigate(TitleFragmentDirections.actionTitleFragmentToPlusGameFragment(pointCorrect,pointIncorrect,menu))
            }
            binding.txtMenuPointCorrect.text = pointCorrect.toString()
            binding.txtMenuPointIncorrect.text = pointIncorrect.toString()
        }
        println("menu "+menu)
        return binding.root
    }

}